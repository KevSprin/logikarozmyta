function [clusters_matrix, distances] = cMeans(cluster_num, weight, inputs, numIter, epsilon)

    inputs_size = size(inputs,1);
    clusters_matrix = randfixedsum(cluster_num, inputs_size, 1, 0, 1)';
    for i = 1:numIter
        % obliczanie centra klastr�w oraz dystanse
        clusters = GetCenters(inputs, weight, cluster_num, inputs_size, clusters_matrix);
        distances = GetDistance(inputs, clusters, cluster_num, inputs_size);
        
        % obliczenie nowej macierzy U oraz aktualizacja
        new_clusters = GetNewUMatrix(weight, distances, cluster_num, inputs_size);

        % warunek ko�cz�cy p�tle
        max_dist = CheckDistances(clusters_matrix, new_clusters);
        if max_dist < epsilon
            break
        end
        clusters_matrix = new_clusters;
    end

end