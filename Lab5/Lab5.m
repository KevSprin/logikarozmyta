clc;
clear;

fileID = fopen('test.txt','r');
formatspec = '%f';
dane_size = [2 inf];
dane = fscanf(fileID, formatspec, dane_size);
dane = dane';

p1 = [7 8];
p2 = [7 16];
p3 = [7 32];

epsilon = 0.001;

liczba_klastrow = 2;
liczba_przykladow = size(dane,1);
waga = 4;
macierz_U(1:liczba_przykladow, 1:liczba_klastrow) = inf;
x = size(macierz_U, 2);

cl(liczba_klastrow, 3) = inf;
cl(1,:) = [1 0 0];
cl(2,:) = [0 1 0];
cl(3,:) = [0 0 1];

hold on;
flag = false;
prev_klastry(1:liczba_klastrow, 1:2) = inf;
num_Iter = 20;
[macierz_U, odleglosci] = cMeans(liczba_klastrow, waga, dane, num_Iter, epsilon);
% for i = 1:num_Iter
%     % obliczanie centra klastr�w oraz dystanse
%     klastry = GetCenters(dane, waga, liczba_klastrow, liczba_przykladow, macierz_U);
%     odleglosci = GetDistance(dane, klastry, liczba_klastrow, liczba_przykladow);
% 
%     if flag == true
%        DrawArrows(klastry, prev_klastry, cl); 
%     end
%     % obliczenie nowej macierzy U oraz aktualizacja
%     nowa_macierz_U = GetNewUMatrix(waga, odleglosci, liczba_klastrow, liczba_przykladow);
%     
%     % warunek ko�cz�cy p�tle
%     max_dist = CheckDistances(macierz_U, nowa_macierz_U);
%     if max_dist < epsilon
%         break
%     end
%     macierz_U = nowa_macierz_U;
%     prev_klastry = klastry;
%     flag = true;
%     scatter(klastry(1,1),klastry(1,2), 100, cl(1,:), '*');
%     scatter(klastry(2,1),klastry(2,2), 100, cl(2,:), '*');
%     %scatter(klastry(3,1),klastry(3,2), 100, cl(3,:), '*');
% end


scatter(klastry(1,1),klastry(1,2), 100, cl(1,:), '*');
scatter(klastry(2,1),klastry(2,2), 100, cl(2,:), '*');
%scatter(klastry(3,1),klastry(3,2), 100, cl(3,:), '*');
for i = 1:size(macierz_U,1)
    arr = macierz_U(i,:);
    [mx, index] = max(arr);
    wpis = dane(i,:);
    scatter(wpis(:,1), wpis(:,2), 40, cl(index,:), 'filled'); 
end


%CheckPoints(p1, klastry, cl);
%CheckPoints(p2, klastry, cl);
%CheckPoints(p3, klastry, cl);

% m = 20;
% results(1:m) = inf;
% for i = 1:m
%     for x = 1:num_Iter
%         % obliczanie centra klastr�w oraz dystanse
%         klastry = GetCenters(dane, i, liczba_klastrow, liczba_przykladow, macierz_U);
%         odleglosci = GetDistance(dane, klastry, liczba_klastrow, liczba_przykladow);
% 
%         % obliczenie nowej macierzy U oraz aktualizacja
%         nowa_macierz_U = GetNewUMatrix(i, odleglosci, liczba_klastrow, liczba_przykladow);
% 
%         % warunek ko�cz�cy p�tle
%         max_dist = CheckDistances(macierz_U, nowa_macierz_U);
%         if max_dist < epsilon
%             break
%         end
%         macierz_U = nowa_macierz_U;
%     end
%     results(i) = CalculateLoss(dane, macierz_U, klastry, i);
% end

hold off;
disp('done');





    
