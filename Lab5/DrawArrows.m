function DrawArrows(klastry, prev_klastry, color)

    for i = 1:size(klastry,1)
        klaster = klastry(i,:);
        prev_klaster = prev_klastry(i, :);
        dp = klaster - prev_klaster;
        quiver(prev_klaster(1), prev_klaster(2), dp(1), dp(2), '-k');
    end
end