function c = GetCenters(dane, waga, liczba_klastrow, liczba_przykladow, macierz_U)
    c(1:liczba_klastrow, 1:2) = inf;
    for j = 1:liczba_klastrow
        licznik = 0;
        mianownik = 0;
        for i = 1:liczba_przykladow
            tmp = dane(i,1:2);
            licznik = licznik + power(macierz_U(i,j),waga) * tmp;
            mianownik = mianownik + power(macierz_U(i,j), waga);
        end
        tmp = licznik/mianownik;
        c(j, 1:2) = tmp;
    end
end