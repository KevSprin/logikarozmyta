function c = CalculateLoss(dane, macierzU, klastry, waga)
    c = 0;
    for i = 1:size(dane,1)
        sum = 0;
        for j = 1:size(klastry,1)
            u = power(macierzU(i,j), waga);
            wpis = dane(i, :);
            klaster = klastry(j,:);
            d = power(norm(wpis - klaster),2);
            result = u * d;
            sum = sum + result;
        end
        c = c + sum;
    end
end