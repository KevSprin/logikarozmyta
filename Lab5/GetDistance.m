function c = GetDistance(dane, klastry, liczba_klastrow, liczba_przykladow)
    c(1:liczba_przykladow, 1:liczba_klastrow) = inf;
    for i = 1:liczba_przykladow
        for j = 1:liczba_klastrow
            x = dane(i, 1:2);
            k = klastry(j,1:2);
            c(i,j) = norm(x - k);
        end
    end
end