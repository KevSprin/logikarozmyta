clc;
clear all;

U = 0:10;
V = 0:10;
Z = 0:10;
% A1 % jedzenia by�o s�abe
% A2 % jedzenie by�o �rednie
% A3 % jedzenie by�o dobre

% B1  % obs�uga by�a s�aba
% B2  % obs�uga by�a �rednia
% B3  % obs�uga by�a dobra 

C1 = trimf(Z, [0 0 4]); % s�aby napiwek
C2 = trimf(Z, [1 5 9]); % �redni napiwek
C3 = trimf(Z, [4 8 inf]); % dobry napiwek

o = length(C1);


kd = KD_Logika();

x = 1;
y = 10;

a1 = A1(x);
a2 = A2(x);
a3 = A3(x);
b1 = B1(y);
b2 = B2(y);
b3 = B3(y);

alfa1 = kd.AND(a1, b1);
alfa2 = kd.AND(a2, b2);
alfa3 = kd.AND(a3, b3);

Wynik_C1(1:o)= inf;
Wynik_C2(1:o)= inf;
Wynik_C3(1:o)= inf;
for z = 1:o
    Wynik_C1(z) = kd.AND(alfa1, C1(z));
    Wynik_C2(z) = kd.AND(alfa2, C2(z));
    Wynik_C3(z) = kd.AND(alfa3, C3(z));
end

C_konc(1:o) = inf;
for z = 1:o
   tmp = kd.OR(Wynik_C1(z), Wynik_C2(z));
   C_konc(z) = kd.OR(tmp, Wynik_C3(z));
end
max = max(C_konc);

licznik = 0;
mianownik = 0;
mianownik2 = 0;
for y = 1:o
    licznik = licznik + (y * C_konc(y));
    mianownik = mianownik + C_konc(y);
    mianownik2 = mianownik2 + y;
end
wynik = licznik/mianownik;
wynik2 = licznik/mianownik2;
disp(wynik);
disp(wynik2);
   
subplot(4,1,1);
plot(Wynik_C1);
title('C1');
subplot(4,1,2);
plot(Wynik_C2);
title('C2');
subplot(4,1,3);
plot(Wynik_C3);
title('C1');
subplot(4,1,4);
plot(C_konc);
title('Wynik');

