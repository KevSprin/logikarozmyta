clc;
clear all;

x = 0:0.1:2*pi;
y = gaussmf(x, [1 3]);
f1 = F1(x);
f2 = F2(x);
hold on;
plot(x, y, x, f1, x, f2);

xlim([0 6]);
ylim([0 2]);

kd = KD_Logika();
n = length(x);
output(1:n) = inf;
counter = 1;
for i = 0:0.1:2*pi
   alfa1 = mfA1(i);
   alfa2 = mfA2(i);
   
   y1 = F1(i);
   y2 = F2(i);
   z = (alfa1*y1 + alfa2*y2)/(alfa1 + alfa2);
   
   output(counter) = z;
   counter = counter +1;
end
plot(x, output);
