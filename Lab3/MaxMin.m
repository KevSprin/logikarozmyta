function c = MaxMin(A, M)
    n = length(A);
    m = length(M);
    tmp_min(1:n) = inf;
    B_prim_min(1:m) = inf;
    for i = 1:m
        for j = 1:n
            tmp_min(j) = min(A(j), M(j,i));
        end
        B_prim_min(i) = max(tmp_min);
    end
    c = B_prim_min;
end