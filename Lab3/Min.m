function c = Min(D, E, F)
    n = length(D);
    Wynik(1:n) = inf;
    for i = 1:n
        Wynik(i) = min([D(i),E(i),F(i)]);
    end
    c = Wynik;
end