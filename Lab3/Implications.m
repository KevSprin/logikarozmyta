classdef Implications
    methods (Static)
        function c = Zadeh(A,B)
            c = max(min(A,B),1-A);
        end
        
        function c = Godel(A,B)
            if (A < B)
                c = 1;
            else
                c = B;
            end
        end
        
        function c = Larsen(A,B)
            c = A * B;
        end
        
        function c = Lukaszewicz(A,B)
            c = min(1, 1 - A + B); 
        end
        
        function c = Mamdani(A,B)
            c = min(A,B);
        end
        
        function c = Binary(A,B)
            c = max(B, 1 - A);
        end
    end
end