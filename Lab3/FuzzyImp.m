function c = FuzzyImp(A, M, ImpFunc)
    n = length(A);
    m = length(M);
    Macierz(1:n, 1:m) = inf;
    for i = 1:n
        for j = 1:m
            Macierz(i,j) = ImpFunc(A(i), M(j));
        end
    end
    c = Macierz;
end