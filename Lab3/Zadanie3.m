clc;
clear all;
U = 0:10;
V = 0:220;
A = [1, 1, 0.8, 0.6, 0.5, 0.2, 0, 0, 0, 0, 0];
%A = [0 0 0 0 0 0 0.1 0.3 0.6 0.8 1];
%A = [0 0 0.1 0.3 0.5 0.8 1 0.8 0.3 0.1 0 0];

M = trimf(V, [0 0 125]);
S = trimf(V, [0 125 220]);
D = trimf(V, [125 220 inf]);


% plot(V, M)
% hold on
% plot(V, S)
% hold on
% plot(V, D)
% hold on
imp = Implications();
% Wynik Zadeh
WynikZadeh_A_M = FuzzyImp(A, M, @imp.Zadeh);
WynikZadeh_A_S = FuzzyImp(A, S, @imp.Zadeh);
WynikZadeh_A_D = FuzzyImp(A, D, @imp.Zadeh);



% Wynik Godel
WynikGodel_A_M = FuzzyImp(A, M, @imp.Godel);
WynikGodel_A_S = FuzzyImp(A, S, @imp.Godel);
WynikGodel_A_D = FuzzyImp(A, D, @imp.Godel);

% Wynik Larsen
WynikLarsen_A_M = FuzzyImp(A, M, @imp.Larsen);
WynikLarsen_A_S = FuzzyImp(A, S, @imp.Larsen);
WynikLarsen_A_D = FuzzyImp(A, D, @imp.Larsen);

% Wynik Łukaszewicz
WynikLukaszewicz_A_M = FuzzyImp(A, M, @imp.Lukaszewicz);
WynikLukaszewicz_A_S = FuzzyImp(A, S, @imp.Lukaszewicz);
WynikLukaszewicz_A_D = FuzzyImp(A, D, @imp.Lukaszewicz);

% Wynik Binary
WynikBinary_A_M = FuzzyImp(A, M, @imp.Binary);
WynikBinary_A_S = FuzzyImp(A, S, @imp.Binary);
WynikBinary_A_D = FuzzyImp(A, D, @imp.Binary);

% Wynik Mamdani
WynikMamdani_A_M = FuzzyImp(A, M, @imp.Mamdani);
WynikMamdani_A_S = FuzzyImp(A, S, @imp.Mamdani);
WynikMamdani_A_D = FuzzyImp(A, D, @imp.Mamdani);

aggregationFunc = @Prod;
% Plotowanie
subplot(3,3,1);
PlotWynik(V, M, S, D, A, WynikZadeh_A_M, WynikZadeh_A_S, WynikZadeh_A_D, 'Subplot 1: WynikZadeh', aggregationFunc);
ylim([0 1]);
subplot(3,3,2);
PlotWynik(V, M, S, D, A, WynikGodel_A_M, WynikGodel_A_S, WynikGodel_A_D, 'Subplot 2: WynikGodel', aggregationFunc);
ylim([0 1]);
subplot(3,3,3);
PlotWynik(V, M, S, D, A, WynikLarsen_A_M, WynikLarsen_A_S, WynikLarsen_A_D, 'Subplot 3: WynikLarsen', aggregationFunc);
ylim([0 1]);
subplot(3,3,4);
PlotWynik(V, M, S, D, A, WynikLukaszewicz_A_M, WynikLukaszewicz_A_S, WynikLukaszewicz_A_D, 'Subplot 4: WynikLukaszewicz', aggregationFunc);
ylim([0 1]);
subplot(3,3,5);
PlotWynik(V, M, S, D, A, WynikBinary_A_M, WynikBinary_A_S, WynikBinary_A_D, 'Subplot 5: WynikBinary', aggregationFunc);
ylim([0 1]);
subplot(3,3,6);
PlotWynik(V, M, S, D, A, WynikMamdani_A_M, WynikMamdani_A_S, WynikMamdani_A_D, 'Subplot 6: WynikMamdani', aggregationFunc);
ylim([0 1]);



