function PlotWynik(V, M, S, D, A, WM, WS, WD, Title, Func)
    Wynik_Y_A_M = MaxMin(A, WM);
    Wynik_Y_A_S = MaxMin(A, WS);
    Wynik_Y_A_D = MaxMin(A, WD);
    Wynik_Y_A = Func(Wynik_Y_A_M, Wynik_Y_A_S, Wynik_Y_A_D);
    plot(V, Wynik_Y_A_M)
    hold on
    plot(V, Wynik_Y_A_S)
    hold on
    plot(V, Wynik_Y_A_D)
    hold on
    plot(Wynik_Y_A)
    title(Title);
end