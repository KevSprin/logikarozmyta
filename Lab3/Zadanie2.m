
U = 0:10;
%A = [1 0.8 0.6 0.3 0.1 0 0 0 0 0 0];
%B = [0 0 0 0 0 0 0.1 0.3 0.6 0.8 1];
A = [1, 1, 0.8, 0.6, 0.5, 0.2, 0, 0, 0, 0, 0];
B = [0, 0, 0, 0, 0, 0.1, 0.3, 0.5, 0.8, 0.9, 1];
n = length(A);

B_prim_min(1:n) = inf;
B_prim_prod(1:n) = inf;

macierzImplikacji = WynikBinary;

tmp_min(1:n) = inf;
tmp_prod(1:n) = inf;
for i = 1:n
    for j = 1:n
        tmp_min(j) = min(A(j), macierzImplikacji(j,i));
        tmp_prod(j) = A(j) * macierzImplikacji(j,i);
    end
    B_prim_min(i) = max(tmp_min);
    B_prim_prod(i) = max(tmp_prod);
end

