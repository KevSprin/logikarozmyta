clc;
clear all;
imp = Implications();

U = 0:10;
A = [1, 1, 0.8, 0.6, 0.5, 0.2, 0, 0, 0, 0, 0];
B = [0, 0, 0, 0, 0, 0.1, 0.3, 0.5, 0.8, 0.9, 1];
n = length(A);


WynikZadeh(1:n, 1:n) = inf;
WynikMamdani(1:n, 1:n) = inf;
WynikGodel(1:n, 1:n) = inf;
WynikLarsen(1:n, 1:n) = inf;
WynikLukaszewicz(1:n, 1:n) = inf;
WynikBinary(1:n, 1:n) = inf;


for i = 1:n
    for j = 1:n
        WynikZadeh(i,j) = imp.Zadeh(A(i), B(j));
        WynikGodel(i,j) = imp.Godel(A(i), B(j));
        WynikLarsen(i,j) = imp.Larsen(A(i), B(j));
        WynikLukaszewicz(i,j) = imp.Lukaszewicz(A(i), B(j));
        WynikBinary(i,j) = imp.Binary(A(i), B(j));
        WynikMamdani(i,j) = imp.Mamdani(A(i), B(j));
    end
end
surf(WynikBinary)
%funkcja_A = gaussmf(U, A);
%plot(U,A)

