function c = T4(a,b)
% T4 - a, b or 0
if b == 1
    c = a;
elseif a == 1
    c = b;
else
    c = 0;
end