function c = N(a)
% N(a) = !a
c = 1 - a;