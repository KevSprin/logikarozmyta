function c = T2(a,b)
% T2 - a*b
c = times(a,b);