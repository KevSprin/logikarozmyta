function c = T3(a,b)
% T3 - max(a + b - 1, 0)
c = max(a + b - 1, 0);