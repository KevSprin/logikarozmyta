function c = S3(a,b)
% S3 = min(a + b, 1)
c = min(a + b, 1);