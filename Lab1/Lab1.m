x = 1:6;
pusty = [0, 0, 0, 0, 0, 0];

C = [0, 0, 0, 0, 1/2, 1];
B = [0, 1/3, 2/3, 1, 1/2, 0];
A = [1, 4/5, 3/5, 2/5, 1/5, 0];

neg_A = N(A);
neg_B = N(B);
% ------ ZADANIE 2 ---------------------------
% ---------- 1 norm ------------------- 
% Sum_A_B = S1(A,neg_B);
% D1 = T1(Sum_A_B, C);
% 
% Sum_A_B = S1(A,B);
% D2 = T1(Sum_A_B, C);
% 
% Sum_A_C = S1(A,C);
% Sum_A_C_B = S1(Sum_A_C, B);
% D3 = T1(neg_A, Sum_A_C_B);
% disp(D1);
% disp(D2);
% disp(D3);
% ==============================================
% ------------ 2 norm

% Sum_A_B = S2(A,neg_B);
% D1 = T2(Sum_A_B, C);
% 
% Sum_A_B = S2(A,B);
% D2 = T2(Sum_A_B, C);
% 
% Sum_A_C = S2(A,C);
% Sum_A_C_B = S2(Sum_A_C, B);
% D3 = T2(neg_A, Sum_A_C_B);
% 
% disp(D1);
% disp(D2);
% disp(D3);

% ===============================================
% ------------- 3 norm
% Sum_A_B = S3(A,neg_B);
% D1 = T3(Sum_A_B, C);
% 
% Sum_A_B = S3(A,B);
% D2 = T3(Sum_A_B, C);
% 
% Sum_A_C = S3(A,C);
% Sum_A_C_B = S3(Sum_A_C, B);
% D3 = T3(neg_A, Sum_A_C_B);
% 
% disp(D1);
% disp(D2);
% disp(D3);
% ================================================
% -------------- 4 norm
% 
% Sum_A_B = S4(A,neg_B);
% D1 = T4(Sum_A_B, C);
% 
% Sum_A_B = S4(A,B);
% D2 = T4(Sum_A_B, C);
% 
% Sum_A_C = S4(A,C);
% Sum_A_C_B = S4(Sum_A_C, B);
% D3 = T4(neg_A, Sum_A_C_B);
% 
% disp(D1);
% disp(D2);
% disp(D3);
% 
% plot(x, Mul_AB_C);
% ===============================================

% -------- ZADANIE 3 ----------------------------
% x  = 1:0.1:6;
% 
% A = trimf(x, [4 6 inf]);
% B = trimf(x, [1 4 6]);
% C = trimf(x, [1 1 6]);
% 
% pusty = zeros(1,length(A));
% 
% neg_A = N(A);
% neg_B = N(B);

% --------- 1 norm --------------------------
% Sum_A_B = S1(A, neg_B);
% D1 = T1(Sum_A_B, C);
% 
% Sum_A_B = S1(A,B);
% D2 = T1(Sum_A_B, C);
% 
% Sum_A_C = S1(A,C);
% Sum_A_C_B = S1(Sum_A_C, B);
% D3 = T1(neg_A, Sum_A_C_B);

% disp(D1);
% disp(D2);
% disp(D3);
% ==========================================
% ------------2 norm------------------------
% Sum_A_B = S2(A,neg_B);
% D1 = T2(Sum_A_B, C);
% 
% Sum_A_B = S2(A,B);
% D2 = T2(Sum_A_B, C);
% 
% Sum_A_C = S2(A,C);
% Sum_A_C_B = S2(Sum_A_C, B);
% D3 = T2(neg_A, Sum_A_C_B);
% 
% disp(D1);
% disp(D2);
% disp(D3);
% ==========================================
% ------------- 3 norm
% Sum_A_B = S3(A,neg_B);
% D1 = T3(Sum_A_B, C);
% 
% Sum_A_B = S3(A,B);
% D2 = T3(Sum_A_B, C);
% 
% Sum_A_C = S3(A,C);
% Sum_A_C_B = S3(Sum_A_C, B);
% D3 = T3(neg_A, Sum_A_C_B);
% 
% disp(D1);
% disp(D2);
% disp(D3);
% ==========================================
% -------------- 4 norm

% Sum_A_B = S4(A,neg_B);
% D1 = T4(Sum_A_B, C);
% 
% Sum_A_B = S4(A,B);
% D2 = T4(Sum_A_B, C);
% 
% Sum_A_C = S4(A,C);
% Sum_A_C_B = S4(Sum_A_C, B);
% D3 = T4(neg_A, Sum_A_C_B);
% 
% disp(D1);
% disp(D2);
% disp(D3);

% Inwolucja
% if A == N(N(A))
%     disp('Inwolucja: TAK');
% else 
%     disp('Inwolucja: NIE');
% end
% 
% % Przemienno��
% if S1(A, B) == S1(B, A) & T1(A,B) == T1(B,A)
%     disp('Przemienno��: TAK');
% else
%     disp('Przemienno��: NIE');
% end
% 
% % ��czno��
% if S1(S1(A,B),C) == S1(A, S1(B,C)) & T1(T1(A,B),C) == T1(A, T1(B,C))
%     disp('��czno��: TAK');
% else
%     disp('��czno��: NIE');
% end
% 
% % Rozdzielno��
% if T1(A, S1(B,C)) == S1(T1(A,B), T1(A,C)) & S1(A, T1(B,C)) == T1(S1(A,B), S1(A,C))
%     disp('Rozdzielno��: TAK');
% else
%     disp('Rozdzielno��: NIE');
% end
% 
% % Idempotencja
% if A == T1(A,A) & A == S1(A,A)
%     disp('Idempotencja: TAK');
% else
%     disp('Idempotencja: NIE');
% end
% 
% % Poch�anianie (absorpcja)
% if A == S1(A, T1(A,B)) & A == T1(A, S1(A,B))
%     disp('Poch�anianie: TAK');
% else
%     disp('Poch�anianie: NIE');
% end
% 
% % Poch�anianie dope�nienia
% if S1(A,T1(N(A),B)) == S1(A,B) & T1(A,S1(N(A),B)) == T1(A,B)
%     disp('Poch�anianie dope�nienia: TAK');
% else
%     disp('Poch�anianie dope�nienia: NIE');
% end
% 
% % Poch�anianie przez zbi�r pusty i U
% if S1(A,x) == x & T1(A, pusty) == pusty
%     disp('Poch�anianie przez zbi�r pusty i U: TAK');
% else
%     disp('Poch�anianie przez zbi�r pusty i U: NIE');
% end    
%     
% % Identyczno��
% if S1(A, pusty) == A & T1(A, x) == A
%     disp('Identyczno��: TAK');
% else
%     disp('Identyczno��: NIE');
% end
%     
% % Prawo Zaprzeczenia
% if T1(A, N(A)) == pusty
%     disp('Prawo Zaprzeczenia: TAK');
% else
%     disp('Prawo Zaprzeczenia: NIE');
% end
% 
% % Prawo wy��czonego �rodka
% if S1(A, N(A)) == x
%     disp('Prawo wy��czonego �rodka: TAK');
% else
%     disp('Prawo wy��czonego �rodka: NIE');
% end
% 
% % Prawo de Morgana
% if N(T1(A,B)) == S1(N(A), N(B)) & N(S1(A,B)) == T1(N(A), N(B))
%     disp('Prawo de Morgana: TAK');
% else
%     disp('Prawo de Morgana: NIE');
% end


