function c = S4(a,b)
% S4 - a, b or 1
if b == 0
    c = a;
elseif a == 0
    c = b;
else
    c = 1;
end