function CheckPoints(p, klastry, cl)
    closest_c = 1;
    tmp_dist = inf;
    wpis = p;
    for c = 1:size(klastry,1)
        klaster = klastry(c,:);
        dist = abs(norm(wpis - klaster));
        if tmp_dist > dist
            tmp_dist = dist;
            closest_c = c;
        end
    end
    scatter(wpis(:,1), wpis(:,2), 60,'MarkerEdgeColor',[0 0 0], 'MarkerFaceColor',cl(closest_c,:), 'LineWidth',1.5); 
end