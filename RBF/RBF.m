function result = RBF(input, centroid, sigma)
% Radial Basis Function usually used for regression
    beta = 1/2*pow2(sigma);
    result = exp(-beta*pow2(norm(input - centroid)));
end