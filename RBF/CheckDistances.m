function c = CheckDistances(U, newU)
    centra = size(U,2);
    przyklady = size(U,1);
    max_dist = 0;
    for i = 1:przyklady
       for j = 1:centra
           dist = abs(newU(i, j) - U(i, j));
           if dist > max_dist
               max_dist = dist;
           end
       end
    end
    c = max_dist;
end