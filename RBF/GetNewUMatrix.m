function c = GetNewUMatrix(waga, odleglosci, liczba_klastrow, liczba_przykladow)
    e = 0.0;
    c(1:liczba_przykladow, 1:liczba_klastrow) = 0.0;
    if (waga-1) == 0
       waga = waga + 0.01; 
    end
    for i = 1:liczba_przykladow
        for j = 1:liczba_klastrow
            sum = 0.0;
            if abs(odleglosci(i,j)) > e
                for k = 1:liczba_klastrow
                   d_licznik = odleglosci(i, j);
                   d_mianownik = odleglosci(i, k);
                   tmp = d_licznik/d_mianownik;
                   expo = 2/(waga-1);
                   result = power(tmp, expo);  
                   sum = sum + result;
                end 
                c(i, j) = 1 / sum;
            else
                for k = 1:liczba_klastrow
                    if j == k
                        c(i,k) = 1;
                    else
                        c(i,k) = 0.0;
                    end
                end
            end
        end
    end
end