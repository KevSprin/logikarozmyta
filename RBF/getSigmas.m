function sigmas = getSigmas(distances)
% Function returning the dispersion (sigma) for each rbf function
    num_inputs = size(distances,1);
    clusters_num = size(distances, 2);
    sigmas(1:clusters_num, 1) = inf; 
    for i = 1:clusters_num
        sum = 0;
        count = 0;
        for j = 1:num_inputs
            row = distances(j, :);
            if row(1, i) == min(row)
                sum = sum + row(1, i);
                count = count + 1;
            end
        end
        sigmas(i, 1) = sum/count;
    end
end