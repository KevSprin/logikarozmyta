function c = GetDistance(dane, klastry, liczba_klastrow, liczba_przykladow, attributes)
    c(1:liczba_przykladow, 1:liczba_klastrow) = inf;
    for i = 1:liczba_przykladow
        for j = 1:liczba_klastrow
            x = table2array(dane(i, 1:attributes));
            k = klastry(j,1:attributes);
            c(i,j) = norm(x - k);
        end
    end
end