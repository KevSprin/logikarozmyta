function [matrix_U, clusters, distances] = cMeans(cluster_num, weight, inputs, numIter, epsilon)

    inputs_size = size(inputs,1);
    attributes = size(inputs,2);
    matrix_U = randfixedsum(cluster_num, inputs_size, 1, 0, 1)';
    for i = 1:numIter
        % obliczanie centra klastr�w oraz dystanse
        clusters = GetCenters(inputs, weight, cluster_num, inputs_size, matrix_U, attributes);
        distances = GetDistance(inputs, clusters, cluster_num, inputs_size, attributes);
        
        % obliczenie nowej macierzy U oraz aktualizacja
        new_matrix_U = GetNewUMatrix(weight, distances, cluster_num, inputs_size);

        % warunek ko�cz�cy p�tle
        max_dist = CheckDistances(matrix_U, new_matrix_U);
        if max_dist < epsilon
            break
        end
        matrix_U = new_matrix_U;
    end

end