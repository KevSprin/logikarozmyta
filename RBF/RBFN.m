clc;
clearvars;

%inputs_per_class = 50; % tylko w iris
inputs_per_class = 25;
attributes = 4;
%iris_table = readtable('iris.csv');
iris_train = readtable('iris.data_tr.txt');
classes = unique(iris_train(:, 5));
% inputs_1 = iris_table(1:50,1:4);
% inputs_2 = iris_table(51:100,1:4);
% inputs_3 = iris_table(101:150,1:4);
inputs_1 = iris_train(1:inputs_per_class, 1:attributes);
inputs_2 = iris_train(inputs_per_class+1:2*inputs_per_class, 1:attributes);
inputs_3 = iris_train(2*inputs_per_class+1:3*inputs_per_class, 1:attributes);

% wykonanie klasteryzacji c-means
liczba_klastrow = 3;
waga = 3;
epsilon = 0.001;
num_Iter = 20;
[macierz_1, klastry_1, odleglosci_1] = cMeans(liczba_klastrow, waga, inputs_1, num_Iter, epsilon);
[macierz_2, klastry_2, odleglosci_2] = cMeans(liczba_klastrow, waga, inputs_2, num_Iter, epsilon);
[macierz_3, klastry_3, odleglosci_3] = cMeans(liczba_klastrow, waga, inputs_3, num_Iter, epsilon);

% dyspersie dla ka�dego centra
sigmy_1 = getSigmas(odleglosci_1);
sigmy_2 = getSigmas(odleglosci_2);
sigmy_3 = getSigmas(odleglosci_3);

% scalanie tablic w jedn�
klastry(1:liczba_klastrow*size(classes,1), 1:attributes) = inf; 
sigmy(1:liczba_klastrow*size(classes,1), 1) = inf;
counter = 0;
for i = 1:liczba_klastrow
    klastry(i+counter,:) = klastry_1(i,:); 
    sigmy(i+counter,1) = sigmy_1(i, 1);
end
counter = counter + liczba_klastrow;
for i = 1:liczba_klastrow
    klastry(i+counter, :) = klastry_2(i,:); 
    sigmy(i+counter,1) = sigmy_2(i, 1);
end
counter = counter + liczba_klastrow;
for i = 1:liczba_klastrow
    klastry(i+counter, :) = klastry_3(i,:); 
    sigmy(i+counter,1) = sigmy_2(i, 1);
end

% przygotowanie tablic id ka�dych z funkcji rbf
klaster_ids(1:liczba_klastrow*size(classes,1), 1) = inf;
counter = 0;
class_id = 1;
for i = 1:liczba_klastrow*size(classes,1)
    if counter >= liczba_klastrow
        counter = 0;
        class_id = class_id + 1;
    end
    klaster_ids(i, 1) = class_id;
    counter = counter + 1;
end


% klasyfikacja zbioru do test�w
iris_test = readtable('iris.data_te.txt');
iris_size = size(iris_test, 1);
iris_mat(1:iris_size, 1:liczba_klastrow*size(classes,1)) = inf;
iris_output_mat(1:iris_size, 1) = inf;
macierz_konfuzji(1:size(classes,1), 1:size(classes,1)) = 0;
correct = 0;
incorrect = 0;
for i = 1:iris_size
    input = table2array(iris_test(i,1:attributes));
    output = table2array(iris_test(i,attributes+1));
    iris_output_mat(i, 1) = output;
    for j = 1:liczba_klastrow*size(classes,1)
        centroid = klastry(j,:);
        sigma = sigmy(j,:);
        suma_centroidow = getSumOfCentroids(input, klastry, sigmy);
        iris_mat(i, j) = NRBF(input, centroid, sigma, suma_centroidow);       
    end
    tmp = iris_mat(i, :);
    [mx, index] = max(tmp);
    wynik = klaster_ids(index, 1);
    if wynik == output
        disp('Prawid�owa klasyfikacja!'); 
        correct = correct + 1;
    else
        disp('Nieprawid�owa klasyfikacja!');
        incorrect = incorrect + 1;
    end
    macierz_konfuzji(wynik, output) = macierz_konfuzji(wynik, output) + 1;
    disp(strcat('Wynik obliczen = ', int2str(wynik), ' a spodziewany wynik to = ', int2str(output)));
end
disp('Macierz konfuzji:');
disp(macierz_konfuzji);

% Obliczanie Accuracy, Precision oraz Recall
accuracy = 100*(correct/iris_size);
n = size(macierz_konfuzji,1);
recall(1:n, 1) = inf;
precision(1:n, 1) = inf;
for i = 1:n
    sum_rec = 0;
    sum_pre = 0;
    for j = 1:n
        if i == j 
            continue;
        end
        sum_rec = macierz_konfuzji(j,i);
        sum_pre = macierz_konfuzji(i,j);
    end
    recall(i,1) = (macierz_konfuzji(i,i) / (macierz_konfuzji(i,i)+sum_rec));
    precision(i,1) = (macierz_konfuzji(i,i) / (macierz_konfuzji(i,i)+sum_pre));
end
disp(strcat('Accuracy = ',num2str(accuracy)));
disp(strcat('Precision (', num2str(100*mean(precision)),'%) : '));
disp(precision);
disp(strcat('Recall (', num2str(100*mean(recall)), '%) : '));
disp(recall);


