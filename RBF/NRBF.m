function probability = NRBF(input, centroid, sigma, centroid_sum)
% normalized Radial Basis Function usually used for classification
    probability = RBF(input, centroid, sigma) / centroid_sum;
end