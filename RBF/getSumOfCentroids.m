function result = getSumOfCentroids(input, centroids, sigmas)
% sum of all RBF function related to some input for normalization (NRBF)
    n = size(centroids, 1);
    result = 0;
    for i = 1:n
        centroid = centroids(i, :);
        sigma = sigmas(i, :);
        result = result + RBF(input, centroid, sigma);
    end
end