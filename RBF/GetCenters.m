function c = GetCenters(dane, waga, liczba_klastrow, liczba_przykladow, macierz_U, attributes)
    c(1:liczba_klastrow, 1:attributes) = inf;
    for j = 1:liczba_klastrow
        licznik = 0;
        mianownik = 0;
        for i = 1:liczba_przykladow
            tmp = table2array(dane(i,1:attributes));
            l = licznik + power(macierz_U(i,j),waga) * tmp;
            m = mianownik + power(macierz_U(i,j), waga);
            licznik = l;
            mianownik = m;
        end
        tmp = licznik/mianownik;
        c(j, 1:attributes) = tmp;
    end
end