clc;
clear all;

p = [0.2 0.3 0.4 0.5];
q = [0 0.1 0.7 0.8 0.9];

%p = 0:0.1:1;
%q = 0:0.1:1;

p_len = length(p);
q_len = length(q);

kd = KD_Logika;
l = L_Logika;

WynikKD_A(1:p_len,1:q_len) = inf; 
WynikL_A(1:p_len,1:q_len) = inf; 
WynikKD_B(1:p_len,1:q_len) = inf; 
WynikL_B(1:p_len,1:q_len) = inf; 
for i = 1:p_len
    for j = 1:q_len
        p_and_q = kd.AND(p(i), q(j));
        p_or_q = kd.OR(p(i), q(j));
        and_q = kd.AND(q(j),p_or_q);
        WynikKD_A(i,j) = kd.OR(p_and_q, and_q);
        
        p_and_q = l.AND(p(i), q(j));
        p_or_q = l.OR(p(i), q(j));
        and_q = l.AND(q(j),p_or_q);
        WynikL_A(i,j) = l.OR(p_and_q, and_q);
        
        p_imp_q = kd.IMP(p(i), q(j));
        WynikKD_B(i,j) =  kd.IMP(p(i),kd.IMP(p(i), p_imp_q));
        
        p_imp_q = l.IMP(p(i), q(j));
        WynikL_B(i,j) =  l.IMP(p(i),l.IMP(p(i), p_imp_q));
    end
end


