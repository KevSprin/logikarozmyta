clc;
clear all;

p = [0 1/2 1];
q = [0 1/2 1];
r  =[0 1/2 1];
p = 0:0.1:1;
q = 0:0.1:1;
r = 0:0.1:1;
n = length(p);
kd = KD_Logika;
l = L_Logika;

WynikKD_1(1:3,1:3) = inf;
WynikL_1(1:3,1:3) = inf;

WynikKD_2(1:3,1:3) = inf;
WynikL_2(1:3,1:3) = inf;

WynikKD_3(1:3, 1:3, 1:3) = inf;
WynikL_3(1:3, 1:3, 1:3) = inf;
for i = 1:n
    for j = 1:n
       % -- Pierwsze zdanie
       WynikKD_1(i,j) = kd.EQ(kd.NOT(kd.AND(p(i), q(j))), kd.OR(kd.NOT(p(i)), kd.NOT(q(j))));
       WynikL_1(i,j) = l.EQ(l.NOT(l.AND(p(i), q(j))), l.OR(l.NOT(p(i)), l.NOT(q(j)))); 
       
       % -- Drugie zdanie
       WynikKD_2(i,j) = kd.IMP(kd.NOT(kd.AND(p(i), q(j))), kd.OR(kd.NOT(p(i)), kd.NOT(q(j))));
       WynikL_2(i,j) = l.IMP(l.NOT(l.AND(p(i), q(j))), l.OR(l.NOT(p(i)), l.NOT(q(j))));
       for k = 1:n
           WynikKD_3(i, j, k) = kd.IMP(kd.IMP(kd.AND(p(i), q(j)), r(k)), kd.IMP(p(i),kd.IMP(q(j),r(k))));
           WynikL_3(i, j, k) = l.IMP(l.IMP(l.AND(p(i), q(j)), r(k)), l.IMP(p(i),l.IMP(q(j),r(k))));
       end
    end
end

