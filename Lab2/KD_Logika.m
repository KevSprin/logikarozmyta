classdef KD_Logika
    methods (Static)
        function c = NOT(a)
        % N(a) = !a
            c = 1 - a;
        end

        function c = AND(a, b)
            c = min(a,b);
        end
        
        function c = OR(a, b)
            c = max(a,b);
        end
        
        function c = IMP(a, b)
            c = max(KD_Logika.NOT(a), b);
        end
        function c = EQ(a, b)
            c = min(KD_Logika.IMP(a, b), KD_Logika.IMP(b, a));
        end
    end
end