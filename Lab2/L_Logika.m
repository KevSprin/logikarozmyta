classdef L_Logika
    methods (Static)
        function c = NOT(a)
           % N(a) = !a
            c = 1 - a;
        end
        
        function c = AND(a, b)
            c = max(0,a+b-1);
        end
        
        function c = OR(a, b)
            c = min(1,a+b);
        end
        
        function c = IMP(a, b)
            c = min(1, b + 1 - a);
        end
        
        function c = EQ(a, b)
            c = max(0, L_Logika.IMP(a,b) + L_Logika.IMP(b, a) - 1);
        end
    end
end