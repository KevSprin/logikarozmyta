clc;
clear all;

p = 0:0.1:1;
q = 0:0.1:1;

% -- Sprawdzi� dla r�nych krok�w
p = [0 1/2 1];
q = [0 1/2 1];

kd = KD_Logika;
l = L_Logika;
n = length(p);
WynikKD_1(1:n,1:n) = inf;
WynikL_1(1:n,1:n) = inf;

for i = 1:n
    for j = 1:n
        % dla KD
        t = kd.IMP(p(i), q(j));
        u = kd.NOT(q(j));
        w = kd.AND(t,u);
        z = kd.IMP(w, kd.NOT(p(i)));
        WynikKD_1(i,j) = z;
        
        % dla �ukaszewicza
        t = l.IMP(p(i), q(j));
        u = l.NOT(q(j));
        w = l.AND(t,u);
        z = l.IMP(w, l.NOT(p(i)));
        WynikL_1(i,j) = z;
    end
end

% s = [0 1/2 1];
% f = [0 1/2 1];
% p = [0 1/2 1];
% o = [0 1/2 1];

s = 0:0.1:1;
f = 0:0.1:1;
p = 0:0.1:1;
o = 0:0.1:1;
WynikKD_2(1:n,1:n,1:n,1:n) = inf;
WynikL_2(1:n,1:n,1:n,1:n) = inf;
for i = 1:n
    for j = 1:n
        for k = 1:n
            for m = 1:n
                % dla KD
                not_s = kd.NOT(s(i));
                not_s_p_imp = kd.IMP(not_s, p(j));
                p_f_imp = kd.IMP(p(j), f(k));
                s_o_imp = kd.IMP(s(i), o(m));
                L = kd.AND(s_o_imp,kd.AND(not_s_p_imp, p_f_imp));
                P = kd.OR(f(k),o(m));
                WynikKD_2(i,j,k,m) = kd.IMP(L,P);   
                
                % dla �ukaszewicza
                not_s = l.NOT(s(i));
                not_s_p_imp = l.IMP(not_s, p(j));
                p_f_imp = l.IMP(p(j), f(k));
                s_o_imp = l.IMP(s(i), o(m));
                L = l.AND(s_o_imp,l.AND(not_s_p_imp, p_f_imp));
                P = l.OR(f(k),o(m));
                WynikL_2(i,j,k,m) = l.IMP(L,P); 
            end
        end
    end
end